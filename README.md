## Programación de Computadores a través de Visores Geográficos FLOSS

### Descripción
Curso de programación de computadores con enfoque orientado a proyectos, específicamente relacionados con la implementación de visores geográficos usando herramientas de software libre o de código abierto. 
Hace parte del conjunto de recursos educativos abiertos del grupo de trabajo OSGEOLabUD para la enseñanza de la programación de computadores a estudiantes de Ingeniería Catastral y Geodesia de la Universidad Distrital Francisco José de Caldas.

## Responsable del Curso
Paulo César Coronado
Grupo de Trabajo Académico OSGeoLabUD
Grupo de investigación GISEPROI

## Contenido Programático
- Introducción a los visores geográficos.
- Fundamentos de HTML y CSS
- Aspectos básicos de Javascript (ES)
- Leaflet
- Cesium
 - GeoJSON
 - CZML
 - OpenLayers
- Aspectos de Backend
 - Bases de datos espaciales
 - Servidores geográficos
 - Node.js
 
 ### Características
- Enfoque orientado a proyectos
- Recursos educativos abiertos
- Javascript (ECMAscript) como lenguaje principal de trabajo.
- Orientado al desarrollo de aplicaciones web y móvil.
