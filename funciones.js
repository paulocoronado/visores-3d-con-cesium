/**
 * Convertir un archivo CSV en formato JSON
 * @param {*} csvFile 
 */

function csv2json(csvFile) {

    //1. Separar el archivo en cada una de las líneas que lo componen
    let csvLines = csvFile.split("\n");

    //2. Crear un arreglo para guardar los resultados
    let objects = [];

    //3. La primera línea contiene el encabezado de las columnas
    let csvHeaders = csvLines[0].split(",");
    

    //4. Recorrer la matriz de líneas. Cada una de ella corresponderá a un objeto.
    for (let i = 1; i < csvLines.length; i++) {

        let myObject = {};
        let currentline = csvLines[i].split(",");

        //5. Recorrer cada una de las columnas para ir colocando 
        //   cada dato como un atributo de los objetos
        for (let j = 0; j < csvHeaders.length; j++) {
            myObject[csvHeaders[j]] = currentline[j];
        }

        objects.push(myObject);

    }

    //6. Convertir la matriz de resultados en formato JSON
    return JSON.stringify(objects);
}

function loadCZML(czmlFile, viewer) {

    viewer.dataSources.add(dataSource);
    viewer.clock.onTick.addEventListener(function(clock) {
      // This example uses time offsets from the start to identify which parts need loading.
      var timeOffset = Cesium.JulianDate.secondsDifference(clock.currentTime, clock.startTime);
  
      // Filter the list of parts to just the ones that need loading right now.
      // Then, process each part that needs loading.
      partsToLoad.filter(function(part) {
        return (!part.requested) &&
          (timeOffset >= part.range[0] - preloadTimeInSeconds) &&
          (timeOffset <= part.range[1]);
      }).forEach(function(part) {
        processPart(part);
      });
    });
  
  }